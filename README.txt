=== Description ===
When you create a webform you can limit the access to specific roles only in the
form settings. By default, only the access to the webform is checked. The node
the webform is attached to will always be visible.

This module ensures that the node the webform is attached to will inherit the
access restriction set to the webform and access to the node itself is also
denied if you view the node with a role that should not have access.

Development of this module is sponsored by LimoenGroen.

=== Installation ===
Download and enable the module. The module has no settings screen.
